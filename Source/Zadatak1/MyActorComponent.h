// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"

#include "AllowWindowsPlatformTypes.h"
#include "Kinect.h"
#include <Kinect.VisualGestureBuilder.h>
#include "HideWindowsPlatformTypes.h"

#include <iostream>
#include <fstream>
#include <direct.h>
#define GetCurrentDir _getcwd

//some often used C standard library header files
#include <cstdlib>
#include <cstdio>
#include <cstring>

//some often used STL header files
#include <iostream>
#include <memory>
#include <vector>
#include <numeric>
#include <cmath>

#include "MyActorComponent.generated.h"

//size of the window
//#define SCRWIDTH 512
//#define SCRHEIGHT 424

//some useful typedefs for explicit type sizes
typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned long long uint64;
typedef short int16;
typedef int int32;
typedef long long int64;

//safe way of deleting a COM object
template<typename T>
void SafeRelease(T& ptr) { if (ptr) { ptr->Release(); ptr = nullptr; } }

struct PCPoint {
	float X;
	float Y;
	float Z;
};

UENUM(BlueprintType)
enum class HandStates : uint8
{
	Lasso UMETA(DisplayName = "Lasso"),
	Opened UMETA(DisplayName = "Opened"),
	Closed UMETA(DisplayName = "Closed"),
	NotTraced UMETA(DisplayName = "NotTraced"),
	UnKnown UMETA(DisplayName = "UnKnown")
};

class KinectStarter
{
public:
	void Init();
	TArray<FVector> Update(float deltaTime);
	void Shutdown();
	bool InitFlag = 0;
	bool handRaisedUp = 0;
	//CameraSpacePoint leftHandPos ;
	CameraSpacePoint leftHandPos;
	CameraSpacePoint rightHandPos;
	HandState leftHandState;
	HandState rightHandState;

	float getAngle(float x1, float y1, float z1, float x2, float y2, float z2);

	float getAngleToKCY(float x1, float y1, float z1, float x2, float y2, float z2);
	float getAngleToKCX(float x1, float y1, float z1, float x2, float y2, float z2);

	int skip = 10;

private:

	IKinectSensor* m_sensor = nullptr;
	IDepthFrameReader* m_depthFrameReader = nullptr;

	IBodyFrameReader* pBodyReader = nullptr;
	//IVisualGestureBuilderFrameSource* pGestureSource[BODY_COUNT];
	//IVisualGestureBuilderFrameReader* pGestureReader[BODY_COUNT];
	IBodyFrameSource* pBodySource;
	//IVisualGestureBuilderDatabase* pGestureDatabase;
	//IGesture* pGesture;
	
	uint16 *m_depthBuffer = nullptr;
	int m_depthWidth = 0, m_depthHeight = 0;


	//coordinate mapper
	//ICoordinateMapper* m_coordinateMapper = nullptr;
	//ColorSpacePoint* m_colorSpacePoints = nullptr;
	//CameraSpacePoint* m_cameraSpacePoints = nullptr;

};

UCLASS(Blueprintable)
class ZADATAK1_API UMyActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UMyActorComponent();

	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Called when the game ends
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason);

	FRotator angleBetween(FVector v1, FVector v2);

	//Kinect instance
	KinectStarter Kinect;
	bool translationFlag = 0;
	bool rotationFlag = 0;
	bool scaleFlag = 0;
	FVector startLeftHandPosition = FVector(0, 0, 0);
	FVector startRightHandPosition = FVector(0, 0, 0);
	FVector startLeftHandPositionForRotation = FVector(0, 0, 0);
	FVector currentLeftHandPosition = FVector(0, 0, 0);
	FVector currentRightHandPosition = FVector(0, 0, 0);
	FVector currentLeftHandPositionForRotation = FVector(0, 0, 0);
	FVector translationValue = FVector(0, 0, 0);
	FVector scaleValue = FVector(0, 0, 0);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "Floats to Vector", CompactNodeTitle = "Floats to Vector", Keywords = "Floats to Vector"), Category = Game)
		static FVector FloatsToVector(float fX, float fY, float fZ);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "Array to Vectors", CompactNodeTitle = "Array to Vectors", Keywords = "Array to Vectors"), Category = Game)
		static TArray<FVector> ArrayToVectors(TArray<int32> IntArray);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "Array 2D to Vectors", CompactNodeTitle = "Array 2D to Vectors", Keywords = "Array 2D to Vectors"), Category = Game)
		static TArray<FVector> Array2DToVectors(TArray<int32> IntArray);

	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "Get Vectors"), Category = Game)
		void GetVectors(const TArray<FVector> & MyVector, const FVector & leftHandPosition, const FVector & angleRot, const FVector & scaleValue);

	UFUNCTION(BlueprintCallable, Category = Game)
		TArray<FVector> LoadPointCloud(const FString& FileName);

	UPROPERTY(BlueprintReadWrite, Category = Game)
		bool translate = 0;

	UPROPERTY(BlueprintReadWrite, Category = Game)
		bool rotate = 0;

	UPROPERTY(BlueprintReadWrite, Category = Game)
		bool scale = 0;

	UPROPERTY(BlueprintReadWrite, Category = Game)
		bool sceneFreezed = 0;

	UPROPERTY(BlueprintReadWrite, Category = Game)
		bool mode = 0;

	UPROPERTY(BlueprintReadWrite, Category = Game)
		bool paste = 0;

	UPROPERTY(BlueprintReadWrite, Category = Game)
		bool select = 0;

	UPROPERTY(BlueprintReadWrite, Category = Game)
		bool innerCut = 0;

	UPROPERTY(BlueprintReadWrite, Category = Game)
		bool pasteAttached = 0;

	UPROPERTY(BlueprintReadWrite, Category = Game)
		bool pasted = 0;

	UFUNCTION(BlueprintPure, meta = (DisplayName = "Position to camera position", CompactNodeTitle = "Position to camera position", Keywords = "Position to camera position"), Category = Game)
		static FVector PosToCameraPos(FVector vector);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "Translate vector", CompactNodeTitle = "Translate vector", Keywords = "Translate vector"), Category = Game)
		static FVector translatePoint(FVector vectorToTranslate, FVector translationValue);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "Rotate point", CompactNodeTitle = "Rotate point", Keywords = "Rotate point"), Category = Game)
		static FVector rotatePoint(FVector translationValue, FVector vectorToRotate, FRotator rotationValue);

	UPROPERTY(BlueprintReadOnly, Category = Game)
		HandStates rightHandState = HandStates::UnKnown;

	UPROPERTY(BlueprintReadOnly, Category = Game)
		HandStates leftHandState = HandStates::UnKnown;

	UPROPERTY(BlueprintReadWrite, Category = Game)
		bool isUpdating = 1;

	UFUNCTION(BlueprintCallable, Category = "Game")
		void writeToLog(FString const & fileIdentificator, FString const & logData);
};

static class CameraParams
{
public:
	const float cx = 254.878f;
	const float cy = 205.395f;
	const float fx = 365.456f;
	const float fy = 365.456f;
	const float k1 = 0.0905474;
	const float k2 = -0.26819;
	const float k3 = 0.0950862;
	const float p1 = 0.0;
	const float p2 = 0.0;
};