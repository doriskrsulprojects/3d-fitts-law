// this file defines certain pieces of information used by the UnrealBuildTool to compile the module.
using System;
using System.IO;
using UnrealBuildTool;

/*
If you mark a function in the Engine module as ENGINE_API, then any module that imports Engine can access that function directly.

The actual API macro equates to one of the following depending on how the code is being compiled by UBT:

__declspec( dllexport ), when compiling module code in modular mode.
__declspec( dllimport ), when including public module headers for a module that you are importing.
empty, when compiling in monolithic mode*/

namespace UnrealBuildTool.Rules
{
	public class FLKinectPlugin : ModuleRules //public class ime_modula
	{
		public FLKinectPlugin(TargetInfo Target) //public ime_modula
		{

            string KinectPathEnvVar = "%KINECTSDK20_DIR%";
            string ExpandedKinectEnvVar = Environment.ExpandEnvironmentVariables(KinectPathEnvVar);
			/*
            //NOTE (OS): Safety check for comptuers that don't have the kinect plugin
            if (KinectPathEnvVar == ExpandedKinectEnvVar)
            {
                var err = "ERROR : Environment variable {0} does not exist in this Windows environment. Check if the Kinect for Windows 2.0 plugin is installed.";
                Console.WriteLine(err, KinectPathEnvVar);
                throw new Exception(err);
            }
			*/
			
            string ThirdPartyKinectIncludePath = Path.Combine(ExpandedKinectEnvVar, "inc");

			ThirdPartyKinectIncludePath = "C:\\Program Files\\Microsoft SDKs\\Kinect\\v2.0_1409\\inc" ;

            string PlatformSpec = (Target.Platform == UnrealTargetPlatform.Win64) ? "x64" : "x86";
            string ThirdPartyKinectLibPath = Path.Combine(ExpandedKinectEnvVar, "Lib", PlatformSpec);

            PublicAdditionalLibraries.Add("C:\\Program Files\\Microsoft SDKs\\Kinect\\v2.0_1409\\Lib\\x64\\Kinect20.lib");

            PublicIncludePaths.AddRange(
				new string[] {
					// ... add public include paths required here ...
				}
				);

			PrivateIncludePaths.AddRange(
				new string[] {
					"FLKinectPlugin/Private", //opseg koji ima, te foldere vidi tijekom builda
                    "FLKinectPlugin/Classes",
                    ThirdPartyKinectIncludePath
					// ... add other private include paths required here ...
				}
				);

            
            
			//engine je baziran na modulima...mislim da dinamicki linka sve module u ue4 koji sadrze ove rijeci (The Core module is a great example -- almost every other module in UE4 specifies Core as a import dependency in their *.Build.cs file.)
			//Dynamically loaded modules are awesome because they can be discovered at startup (kind of like a plugin), and often can be reloaded on the fly.
			PublicDependencyModuleNames.AddRange(
				new string[]
				{
					"Core",
					"CoreUObject",
					// ... add other public dependencies that you statically link with here ...
				}
				);

			PrivateDependencyModuleNames.AddRange(
				new string[]
				{
					"Engine",
                    // ... add private dependencies that you statically link with here ...
				}
				);

			DynamicallyLoadedModuleNames.AddRange(
				new string[]
				{
					// ... add any modules that your module loads dynamically here ...
				}
				);
		}
	}
}