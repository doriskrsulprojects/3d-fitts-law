#pragma once
#include "KinectFunctionLibrary.generated.h" //#include "ClassName.generated.h"

//This file includes any header files - including the module's auto-generated headers - necessary to compile the classes contained within the module.
//sastoji se od funkcija i varijabli koje se onda koriste u private klasama (koje izvrsavaju modul, ovdje ga samo definiramo)

//Enums basically give you ability to define a series of related types with long human-readible names, using a low-cost data type.
//These could be AI states, object types, ammo types, weapon types, tree types, or anything really


//You need to add the UENUM definition above your class and then actually create a member variable in your class that you want to have be an instance of this enum.
UENUM(BlueprintType) //"BlueprintType" is essential to include
enum class EFLKinect_HandState : uint8
{
	//u kodu s :: pristupamo ovim varijablama
	Unknown = 0, NotTracking = 1, Open = 2, Closed = 3, Lasso = 4
};

UENUM(BlueprintType)
enum class EFLKinect_JointType : uint8
{
	SpineBase = 0,
	SpineMid = 1,
	Neck = 2,
	Head = 3,
	ShoulderLeft = 4,
	ElbowLeft = 5,
	WristLeft = 6,
	HandLeft = 7,
	ShoulderRight = 8,
	ElbowRight = 9,
	WristRight = 10,
	HandRight = 11,
	HipLeft = 12,
	KneeLeft = 13,
	AnkleLeft = 14,
	FootLeft = 15,
	HipRight = 16,
	KneeRight = 17,
	AnkleRight = 18,
	FootRight = 19,
	SpineShoulder = 20,
	HandTipLeft = 21,
	ThumbLeft = 22,
	HandTipRight = 23,
	ThumbRight = 24,
	//HACK (OS): this compiler doesn't support 	Count = (ThumbRight + 1)
	Count = 25
};

UENUM(BlueprintType)
enum class EFLKinect_TrackingState : uint8
{
	Not_Tracked = 0, Inferred = 1, Tracking = 2
};

//sluzi za organizaciju podataka, metode su po defaultu public
//ability to have functions for internal data type operations, The idea of USTRUCTS() is to declare engine data types that are in global scope and can be accessed by other classes/structs/blueprints
USTRUCT(BlueprintType) //BlueprintType omogucava vidljivost u BP
struct FFLKinect_Body
{
	/*
	 UPROPERTY([specifier, specifier, ...], [meta=(key=value, key=value, ...)])
	 Type VariableName;  varijabla s dodatnim opisima
	*/
	GENERATED_BODY()
		static const uint8 nJoints{ static_cast<uint8>(EFLKinect_JointType::Count) };
	//Always make USTRUCT variables into UPROPERTY()
	//any non-UPROPERTY() struct vars are not replicated
	UPROPERTY(BlueprintReadOnly) EFLKinect_HandState HandLeftState;
	UPROPERTY(BlueprintReadOnly) EFLKinect_HandState HandRightState;
	FVector positions[nJoints];
	FRotator orientations[nJoints];
	EFLKinect_TrackingState trackingStates[nJoints];
	//Always initialize your USTRUCT variables!
};

/*
UCLASS([specifier, specifier, ...], [meta(key=value, key=value, ...)])
class ClassName : public ParentName (klasa koju nasljeduje)
{
    GENERATED_BODY()
	//var i funk koje nasljeduju nju
}

*/

UCLASS()
class FLKINECTPLUGIN_API UKinectPluginFunctionLibrary : public UBlueprintFunctionLibrary  //parent knjiznica se nalazi: C:\Program Files (x86)\Epic Games\4.12\Engine\Source\Runtime\Engine\Classes\Kismet
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "Kinect")
		static FVector GetJointPosition(UPARAM(ref) FFLKinect_Body const& body, EFLKinect_JointType type);

	UFUNCTION(BlueprintPure, Category = "Kinect")
		static FRotator GetJointOrientation(UPARAM(ref) FFLKinect_Body const& body, EFLKinect_JointType type);

	UFUNCTION(BlueprintPure, Category = "Kinect")
		static EFLKinect_TrackingState GetTrackingState(UPARAM(ref) FFLKinect_Body const& body, EFLKinect_JointType type);

	UFUNCTION(BlueprintCallable, Category = "Kinect")
		static void GetKinectTrackedBodies(TArray<bool>& out);

	UFUNCTION(BlueprintCallable, Category = "Kinect")
		static void GetKinectBody(uint8 number, FFLKinect_Body& out);

	UFUNCTION(BlueprintCallable, Category = "Kinect")
		static bool IsKinectBodyTracked(uint8 number);

	UFUNCTION(BlueprintCallable, Category = "Kinect|Debug")
		static bool GetFirstActiveKinectBody(FFLKinect_Body& out);

	UFUNCTION(BlueprintCallable, Category = "Kinect|Debug")
		static void GetAllPositions(uint8 num, TArray<FVector>& out);

	UFUNCTION(BlueprintCallable, Category = "Kinect|Debug")
		static void DrawBody(const AActor* worldActor, UPARAM(ref) FFLKinect_Body& body, FTransform transform, float PointSize, float LineLength);
	
/*	UFUNCTION(BlueprintCallable, Category = "Kinect")
		static void InitializationOfSenzors();*/
};